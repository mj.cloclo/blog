---

title: About
menu: main

---

👋 I'm James Ramsay. I am a follower of Christ, do Product at [Remote](https://remote.com/), and enjoy learning and making things.

Previously:

- [GitLab](https://about.gitlab.com/) with a focus on code review and [Git](https://git-scm.com) storage
- [Adslot](https://adslot.com/) to make buying digital display advertising simple and efficient by bypassing the RTB networks

Feel free to reach out on [Twitter](https://twitter.com/_jramsay) or via [email](mailto:me@jramsay.com.au).

Theme forked from [Marfa](https://github.com/microdotblog/theme-marfa).
Generated with [Hugo](https://gohugo.io).
Source on [GitLab](https://gitlab.com/jramsay/blog).
