---

title: Projects

---

**Digital artifacts**

I am the author and maintainer of the following open source tools:

- _In progress_ **[GitLab CLI](https://gitlab.com/jramsay/git-lab):** a command line tool for checking GitLab commit statuses, CI traces and more.
- _In progress_ **[PNGBot](https://gitlab.com/jramsay/pngbot):** automated compression of PNG images in the CI pipeline.
- _In progress_ **[DeLorean](https://gitlab.com/jramsay/delorean):** scripts for rewriting Git history in real time.
- **[Hercule](https://github.com/jamesramsay/hercule):** simple document transclusion, ideal for Markdown documents.

**Physical artifacts**

I enjoy cooking and making things with my hands:

- **Ultralight Duffle Bag:** made from Dyneema Composite Fabric (DCF).
- **Shrubs**, **[Pickles](https://www.instagram.com/p/BixOW8ID8L-)** and **[Sourdough](https://www.instagram.com/p/BIOzn0EDO2e).** Currently I favour a poolish rye starter, with a blend of white and whole wheat flours for my sourdough.
- **[Picture Frames](https://www.instagram.com/p/pxX-kYuANA),** coffee table and someday soon cabinets and chairs.
