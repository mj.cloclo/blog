---

title: Podcasts
url: _private/podcasts
private: true

---

Private podcasts of audio files that are not available publicly.

At build this section is moved to a different URL scheme:

```
HUGO_PERMALINKS_PODCAST="/foo/bar/:filename"
```

## Podcast post type

`index.md` frontmatter

```yaml
private: true
title: "Title of the podcast"
link: http://example.com/audio-series
image: http://example.com/cover.jpg # otpional
resources:
- src: episodes.yml # required
- src: cover.jpg # optional
media_host: dropbox # aws | dropbox
outputs:
- podcast
```

`episodes.yml`

```yaml
- id: SHA256 of file location
  type: audio/x-m4a # audio/mpeg | audio/x-m4a
  title: episode title
  author: name
  date: "YYYY-MM-DD"
```

## AWS S3

Expected environment variables:

- `AWS_AUTH_SECRET_KEY`
- `AWS_AUTH_KEY_ID`
- `AWS_S3_HOST`

## Dropbox

Expected environment variables:

- `DROPBOX_AUTH_TOKEN`
