---

private: true
title: "[Redeemer] James: A Faith that Comes Down to Earth"
date: 1992-10-01
link: https://gospelinlife.com/downloads/james-a-faith-that-comes-down-to-earth/
image: https://gospelinlife.com/wp-content/uploads/edd/2018/02/Series_James_A_Faith_that_Comes_Down_to_Earth-400x400.jpg
episodes: "dropbox://rpc_james/episodes.yml"
outputs:
- podcast

---

{{< podcast_links >}}
