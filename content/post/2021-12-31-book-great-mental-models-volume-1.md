---

slug: 'book-great-mental-models-volume-1'
type: post
date: 2021-12-31T13:04:22+11:00
categories:
- Books

---

Finished reading: [The Great Mental Models Volume 1: General Thinking Concepts](https://www.amazon.com/dp/1999449002/) by [Farnam Street](https://fs.blog) 📚

