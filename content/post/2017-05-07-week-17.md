---

title: 'Week 18, 2017'
slug: week-17
type: post

---

A day late and brief because I spent the weekend cycling!

---

In the past few weeks I've been working on creating an environment for reproducible data analysis with Docker, Jupyter and R. What I thought would be a simple project turned quickly became complicated when I had to work out how to upgrade Java and R's Java bindings to connect to AWS Athena. I hope to write a longer post about this project soon.

---

Last week Jan Chipchase released [The Field Study Handbook](https://www.kickstarter.com/projects/janchipchase/the-field-study-handbook) which you should definitely read more about and probably back. I'm looking forward to spending some quality time reading it once it ships, currently estimated for June 2017.
