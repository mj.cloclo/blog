---

title: 'The resolution of the Bitcoin experiment'
link: 'https://medium.com/@octskyward/the-resolution-of-the-bitcoin-experiment-dabb30201f7'
people: ['Mike Hearn']
slug: resolution-of-the-bitcoin-experiment
type: post

---

Mike Hearn (previously Bitcoin Core developer):

> It [Bitcoin] has failed because the community has failed. What was meant to be a new, decentralised form of money that lacked “systemically important institutions” and “too big to fail” has become something even worse: a system completely controlled by just a handful of people. Worse still, the network is on the brink of technical collapse. The mechanisms that should have prevented this outcome have broken down, and as a result there’s no longer much reason to think Bitcoin can actually be better than the existing financial system.

After mining a small quantity of Bitcoin early last year, I became more aware of an elite group who were responsibly for an increasing majority of mining and had accumulated a vast wealth in Bitcoin. My interest in Bitcoin and cryptocurrency is the possibility of improving the ability of disadvantaged people to access cheap, if not free, financial services such as banking and international transfers. The growing control of the currency by a small group of people runs counter to such an interest, so I cashed out my $100.

> In other cases, entire datacenters were disconnected from the internet until the single XT node inside them was stopped. About a third of the nodes were attacked and removed from the internet in this way.

> Worse, the mining pool that had been offering BIP101 was also attacked and forced to stop. The message was clear: anyone who supported bigger blocks, or even allowed other people to vote for them, would be assaulted.

The criminal activity and tightening grip by a small handful of people described by Hearn reveals how far Bitcoin has fallen from the goals and ideas the experiment promised. The promise of sharing power and money has been seized by this small few.

Hopefully other cryptocurrency experiments will be able to learn from the failures of Bitcoin.
