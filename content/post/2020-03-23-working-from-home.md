---

slug: working-from-home
date: 2020-03-23T05:53:12.078106051Z
photos:
  - /uploads/2020/bCyoyn9Zg.jpg
categories:
  - Photos

---

Working from home certainly makes baking bread convenient. I’m currently using bakers yeast and a poolish preferment in my country loaf for simplicity.
