---

slug: topkapi
date: 2019-05-21T11:40:27Z
location: Istanbul, Turkey
photos:
  - /uploads/2019/nPtriZxjt.jpg
  - /uploads/2019/RQ6pI2HlP.jpg
  - /uploads/2019/SXrJodoxO.jpg
  - /uploads/2019/s2mp6ly0m.jpg
categories:
  - Photos

---

Dream home tiles and textile inspiration in Istanbul with Mim two weeks ago 😍
