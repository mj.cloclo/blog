---

slug: 'book-good-economics-for-hard-times'
type: post
date: 2022-05-02T13:02:10+10:00
categories:
- Books

---

Finished reading: [Good Economics for Hard Times](https://www.amazon.com/dp/0141986190/) by  Abhijit V. Banerjee and Esther Duflo 📚

