---

slug: 'book-less-is-more'
type: post
date: 2022-01-24T18:40:41+11:00
categories:
- Books

---

Finished reading: [Less is More: How Degrowth Will Save The World](https://www.amazon.com/dp/1786091216/) by Jason Hickel 📚

