---

title: Bitcoin Energy Consumption Index
link: https://digiconomist.net/bitcoin-energy-consumption
type: post

---

Bitcoin's rising price is driving even higher electricity consumption, as people seek to find their nugget of digital gold. Alex de Vries estimates _Bitcoin accounts for 0.12% of global electricity consumption_ and would be ranked 68 in global energy consumption if it were a country. The wastefulness is grotesque. Regardless of the merits of crypto currencies, Bitcoin [doesn't scale](https://blockchain.info/charts/avg-confirmation-time?scale=1)—no one wants to wait minutes or hours at the register for a transaction to clear—and worse its energy requirements cause tonnes of carbon dioxide to be emitted. Research and innovation in crypto currencies should absolutely continue, but it's time Bitcoin was shut down.
