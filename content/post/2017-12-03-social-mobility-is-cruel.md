---

slug: 'social-mobility-is-cruel'
link: https://www.thersa.org/discover/publications-and-articles/matthew-taylor-blog/2015/10/what-we-nearly-all-agree-is-wrong-again
type: post

---


[Matthew
Taylor](https://www.thersa.org/discover/publications-and-articles/matthew-taylor-blog/2015/10/what-we-nearly-all-agree-is-wrong-again) writing in 2015:

> Social mobility (starting gate equality) is often cited an acceptable alternative to the more left wing idea of egalitarianism (end point equality), yet it is clear that the best way to create a meritocracy is to pursue greater egalitarianism. Mobility is greater in societies that are less unequal partly because the rungs in the ladder of stratification are closer together and partly because middle class people are less frightened of the consequences of downward mobility _(generally the barrier to mobility is less about the poor's ability to go up and more about the resistance of the well off to going down)._

[Watch at The Guardian](https://www.theguardian.com/commentisfree/video/2016/feb/03/social-mobility-is-cruel-heres-why-video?CMP=embed_video)
