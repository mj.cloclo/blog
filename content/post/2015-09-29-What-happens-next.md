---

title: 'What Happens Next Will Amaze You'
link: 'http://idlewords.com/talks/what_happens_next_will_amaze_you.htm'
people: ['Maciej Cegłowski']
slug: what-happens-next
type: post

---

[Maciej Cegłowski's](http://idlewords.com/) presented at a conference in Copenhagen, Denmark:

> Today we live in a Blade Runner world, with ad robots posing as people, and Deckard-like figures trying to expose them by digging ever deeper into our browsers, implementing Voight-Kampff machines in Javascript to decide who is human. We're the ones caught in the middle.

> The ad networks' name for this robotic deception is 'ad fraud' or 'click fraud'. (Advertisers like to use moralizing language when their money starts to flow in the wrong direction. Tricking people into watching ads is good; being tricked into showing ads to automated traffic is evil.)

The talk includes numerous interesting observations around the evolution of ad tech and the genesis of robots/ad fraud. It's worth reading through his argument as it unpacks many of the unpleasant incentives for ad fraud and the impact of the technologies that exist to track and mitigate ad fraud.

Further on he proposes 6 fixes, the most interesting being _a ban on third-party ad tracking._

This sort of ban would trigger an incredible reset on the way ad technology works and immediately start moving ad tech is a much more consumer friendly direction. Indeed, third-party tracking is the raison d'être for iOS9 content blockers.
