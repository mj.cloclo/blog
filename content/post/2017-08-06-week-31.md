---

title: 'Week 31, 2017'
slug: week-31
type: post

---

Last week was a lovely mix of family and cycling, the perfect way to decompress. I hope to spend more time reading this week, although I did read Tolstoy's Happy Ever After last week. I'm planning a few long rides for next week to Ballarat and perhaps also from Ballan to Geelong.

---

[This Is How Big Oil Will Die](https://shift.newco.co/this-is-how-big-oil-will-die-38b843bd4fe0) was an interesting read this week. The fact that electric vehicles have a much longer lifespan and lower cost of running is going to make the demise of combustion engine vehicles rapid. At this point, I'm not sure if I will ever own a car. Although, it isn't clear to me how car sharing works when you have small children (car seats) or want to put a bicycle on the roof.

---

I switched back to vim, specifically [neovim](http://neovim.io), last year. At worst I am equally productive and have enjoyed improved performance and battery life, particularly on my tired mid-2011 MBA. [This article](https://medium.freecodecamp.org/why-i-still-use-vim-67afd76b4db6) shows how poorly Electron based editors [Atom](https://atom.io) and [Code](https://code.visualstudio.com) perform compared to vim on startup and memory usage.

Relatedly, Firefox 58 is due out later this year boasting better performance. I'll certainly give it a good try but it'll face stiff competition from Safari. I switched from Chrome to Safari for battery life, but have grown to really appreciate the iCloud integration between my phone and laptop. Although, with a newer laptop that supports Handoff, this might be mitigated.

---

From **The List of Articles to be Read**, Mary Cook's article [Git from the inside out](https://maryrosecook.com/blog/post/git-from-the-inside-out) is top of my list for this week. My understanding of git is primarily based on its API, and I am very keen to better understand its internals.
