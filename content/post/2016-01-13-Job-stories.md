---

title: 'Designing Features Using Job Stories'
link: 'https://blog.intercom.io/using-job-stories-design-features-ui-ux/'
people: ['Alan Klement']
slug: job-stories
type: post

---

Alan Klement, guest post for Intercom:

> The casualties of this waterfall process are the subtleties which it is necessary to understand when creating great products: causality, anxieties, and motivations. As development teams recognize that they need to be close to customers, it’s also appropriate to consider better ways of leveraging customer empathy to create products.

> This philosophy of focusing on causality, anxieties, and motivations is called Jobs To Be Done, and a granular way to bring this concept into a product is to use Job Stories to design features, UI, and UX.

Causality, anxieties, and motivations are critical for effective product design and development. We have also found that traditional personas, user stories and use cases typically fail to capture this important information.

At Adslot, I've worked to counter this by frequently including a "rationale" alongside the user story. Ultimately, however, the most effective way we've developed to communicate this is in early stage _feature discovery meetings_ with developers, design and QA.
