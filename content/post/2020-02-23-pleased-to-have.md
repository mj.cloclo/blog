---

date: 2020-02-23T10:25:09.754216367Z
slug: pleased-to-have

---

Pleased to have added an experimental Micropub endpoint so I can post here more easily. This was posted directly to my blog from the Micro.blog app. 🎉
