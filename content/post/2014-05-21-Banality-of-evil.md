---

title: 'The Banality of Evil: Proposed Amendments to the Racial Discrimination Act'
people: ['Meleesha Bardolia']
slug: banality-of-evil
type: post

---

Meleesha Bardolia, writes for Right Now, on proposed changes to Australia's Racial Discrimination Act:

> Brandis’s packaging of these revisions as innocuous, at least, and progressive, at best, obfuscates the transference of power from victim to perpetrator in an act originally designed to ‘protect’ individuals and groups against discrimination. This is suggested by the limitations imposed on the scope of racial discrimination to “vilification” and “intimidation”. The new Act defines the term “vilify” as meaning to incite hatred against a person or a group of persons. This definition shifts the onus of the burden of proof from the victim to the perpetrator. Furthermore, the word “intimidate” is defined as causing fear of physical harm.

Meleesha outlines four key changes which 'legalise the dehumanisation of groups and individuals based on their racial extraction.'

It is definitely worth reading in it's entirety:  [The Banality of Evil: Proposed Amendments to the Racial Discrimination Act](http://rightnow.org.au/writing-cat/article/the-banality-of-evil-proposed-amendments-to-the-racial-discrimination-act/) (Right Now, 16 May 2014)
