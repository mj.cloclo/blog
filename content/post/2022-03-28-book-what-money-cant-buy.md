---

slug: 'book-what-money-cant-buy'
type: post
date: 2022-03-28T21:01:53+11:00
categories:
- Books

---

Finished reading: [What Money Can't Buy: The Moral Limits of Markets](https://www.amazon.com/dp/0241954487/) by Michael Sandel 📚

