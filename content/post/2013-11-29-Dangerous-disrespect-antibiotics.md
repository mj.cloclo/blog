---

title: 'Our dangerous disrespect of antibiotics'
link: 'https://medium.com/p/892b57499e77'
people: ['Maryn McKenna']
slug: dangerous-disrespect-antibiotics
type: post

---

Maryn McKenna writes [Imagining the Post-Antibiotics Future](https://medium.com/p/892b57499e77):

> I imagine what he might have thought  [...]  if he had known that a few years later, his life could have been saved in hours. I think he would have marveled at antibiotics, and longed for them, and found our disrespect of them an enormous waste. As I do.

It worries me that more people aren't aware of the harm caused to society by not correctly adhering to prescriptions. Maryn McKenna's article clearly explains the importance of taking the issue seriously.
