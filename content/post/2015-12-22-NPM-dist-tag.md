---

title: 'Shipping stable npm releases'
link: 'https://medium.com/greenkeeper-blog/one-simple-trick-for-javascript-package-maintainers-to-avoid-breaking-their-user-s-software-and-to-6edf06dc5617'
slug: npm-dist-tag
type: post

---

Another post where the understanding the detail of the tools we use everyday can make a very measurable difference. Greenkeeper use npm and Babel 6 as examples of how using distribution tags can help ensure stable module releases in their article [One simple trick for JavaScript package maintainers to avoid breaking their user’s software and to ship stable releases](https://medium.com/greenkeeper-blog/one-simple-trick-for-javascript-package-maintainers-to-avoid-breaking-their-user-s-software-and-to-6edf06dc5617). Although I've used a moderate portion of npm's capabilities, I'd missed this really useful feature.
