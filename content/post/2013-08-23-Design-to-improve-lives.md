---

title: 'Design to improve lives'
people: ['Enrique Allen']
slug: design-to-improve-lives
type: post

---

Enrique Allen writes in the interview Learn by Doing for [Kern and Burn](http://kernandburn.com/):

> I've seen too many talented people in Silicon Valley work on products that are incremental or not that life changing for anyone. By using the term 'designer,' we have a responsibility, even a moral obligation, to intentionally impact people’s lives, hopefully for the better. Unfortunately, the products we design often waste people’s valuable resources and their attention, which is more scarce than money and time. Why is it that there are so many talented people working on shallow problems?
