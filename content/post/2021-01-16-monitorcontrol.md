---

title: MonitorControl
slug: 'monitorcontrol'
link: https://github.com/MonitorControl/MonitorControl
type: post
categories:
- Tech

---

> Control your external monitor brightness, contrast or volume directly from a menulet or with keyboard native keys.

Why isn't [MonitorControl](https://github.com/MonitorControl/MonitorControl) a built-in feature of MacOS?
