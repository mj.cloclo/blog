---

date: 2020-03-09T22:05:01.376065233Z
slug: yesterday-i-setup

---

Yesterday I setup Minio on my Synology DS216play using [MinioSPK](https://github.com/darkmuggle/MinioSPK) for use with [Arq](https://www.arqbackup.com/blog/synology-backup-guide/). So far, faster and more reliable than using [Autofs](/2017/11/29/macos-autofs/).
