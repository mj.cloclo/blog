---

title: 'Delete labeled emails after 30 days (Gmail tip)'
slug: automatically-delete-old-emails
type: post

---

Today I discovered I'd been accumulating huge amounts of unimportant emails.
Emails that I needed to keep for 30 days, but after which are completely meaningless.
Unfortunately, deleting millions (over 1,000 per day over 4 years) of email messages isn't easily achieved.

Thankfully, [Google Script](http://script.google.com) allows you to interact with Google Apps (including Google Mail) using Javascript executed on Google servers to perform tasks, even timer based tasks like CRON.
This makes running a task daily to trash old emails simple!

[↪ Check out the script here on Github Gist](https://gist.github.com/jamesramsay/9298cf3f4ac584a3dc05).
