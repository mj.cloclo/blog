---

title: PostgreSQL Dynamic unpivot
slug: 'sql-dynamic-unpivot'
link: https://blog.sql-workbench.eu/post/dynamic-unpivot/
type: post
date: 2021-12-06T11:42:21+11:00
categories:
- Tech

---


A clever dynamic solution to the unpivot problem from [Thomas Kellerer](https://blog.sql-workbench.eu/post/dynamic-unpivot/):

```sql
WITH data_percentiles AS (
  SELECT
    month,
    percentile_disc(0.50) within group (order by count) "50p",
    percentile_disc(0.80) within group (order by count) "80p",
    percentile_disc(0.99) within group (order by count) "99p"
  FROM data
  GROUP BY 1
)

SELECT
  p.month,
  d.series,
  d.amount::numeric
FROM data_percentiles p
CROSS JOIN LATERAL jsonb_each_text(to_jsonb(p) - 'month') as d(series, amount)
ORDER BY 1, 2
```
