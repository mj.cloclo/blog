---

title: 'The value formalising process and ideas'
slug: formalising-ideas-and-processes
type: post

---

I am very lucky to work in a great product and engineering team at Adslot, which embraces continuous improvement and encourages experimentation to create a better working environment and build better products for our customers. Over the last four years at Adslot we've tweaked, polished and overhauled many aspects of development, QA, product and UX processes and tooling. Besides what we’ve learned from the individual experiments, more recently I’ve been increasingly appreciating _the value of formalising the processes and ideas that stick_. This seems painfully obvious in hindsight.

I think formalisms are helpful because they assist processes and ideas be communicated more effectively by providing a common language for the underlying ideas and motivations. This also makes it easier to critique, improve and apply to new contexts.

This realisation particularly struck me over the last few months when we became aware that we’d independently arrived at an engineering culture and product processes similar to some great companies we respect. It was satisfying to realise we were on ground already tread by other great companies, but my real excitement has come from how we’ve been able to improve how we communicate our processes and culture across the business. This is particularly useful at Adslot where we are trying to build a common culture and shared processes across many teams distributed around the world.
