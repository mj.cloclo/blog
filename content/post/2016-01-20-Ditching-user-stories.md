---

title: The Problem With User Stories and What's Better
link: 'http://alanklement.blogspot.com.au/2013/03/the-problem-with-user-stories-and-whats.html'
people: ['Alan Klement']
slug: ditching-user-stories
type: post

---

Alan Klement:

> When I used to write user stories with my team, my teammates would read them, think they understand them and then go on with their own interpretation of the story, which ended up being different than mine.  I started noticing the problem was with the user stories, and not me, when different engineers would interpret the same story very differently.

> _**So I did something different: I stopped writing them.**_

> At first the engineers were confused, so they would come to me and mention the story was missing.  Then something amazing would happen...we would talk about it! Sometimes, several of us would talk about it together. After a while everyone got into it, team collaboration took off and the product became better. We would talk about everything frequently.

Reading this article was like déjà vu!
Over the last 12 months I've also dialed back on writing user stories,
shifted to a discussion driven approach,
and experienced a similar increase in collaboration and understanding of the underlying problems we are solving.

Answering the question _why?_ is so much more valuable than any list of user stories.
It empowers and enables the entire team to participate in the process of solving the problem,
rather than keeping this important information siloed with individual members of the product and UX team.
