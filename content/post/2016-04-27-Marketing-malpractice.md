---

title: 'Marketing Malpractice: The Cause and the Cure'
link: 'https://hbr.org/2005/12/marketing-malpractice-the-cause-and-the-cure'
people: ['Clayton Christensen']
slug: marketing-malpractice
type: post

---

From the HBR archives, this is one of the classic papers behind [Jobs To Be Done (JTBD)](http://www.jobstobedone.info/):

> In other words, _the job, not the customer, is the fundamental unit of analysis_ for a marketer who hopes to develop products that customers will buy.

I've recently been reading the [The Clayton M. Christensen Reader](https://hbr.org/product/the-clayton-m-christensen-reader/15003-PBK-ENG) and am enjoying the time spent thinking through the ideas behind the processes and tools of JTBD.

If you've never read any of Clayton Christensen's papers but find JTBD interesting, reading this paper would be time well spent.
