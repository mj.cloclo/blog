---

title: 'Privacy and security'
slug: comments-on-privacy-and-security
type: post

---

It has been interesting reading the stream of articles since the [FBI requested Apple to assist recover data from the iPhone](http://www.apple.com/customer-letter/) seized from attack in San Bernardino.
No doubt a serious legal precedent will be set, but as many have pointed out the physical existence of a modified operating system allowing faster brute force access to an iPhone is far from ideal.

One of the aspects I appreciate most of my iPhone is it's security, simply because I rely on my mobile in day to day life including banking and paying bills.
But it's not just financial information that is valuable.
Having confidence other types of personal information is secure is important, particularly as social engineering based attacks become increasingly common.

As technology connects us, allowing to share increasingly personal and confidential information on our mobile devices, security needs to keep up.
Government mandated backdoors or weakened encryption prevent any such lock-step.
