---

title: 'Thoughts on Header Bidding'
slug: thoughts-on-header-bidding
type: post

---

Header Bidding has been receiving the sustained attention of the online advertising press for a number of months now.
It is a clever hack which allows price prioritisation of RTB exchange bids and traditional line items within a publisher ad server, which would otherwise prioritise RTB last. Header bidding is then a helpful tool allowing publishers to increase yield of sub-guaranteed priority and incrementally increases impression volumes entering the exchanges.

The interest in header bidding, however, has primarily centred on upending prioritisation—the waterfall—entirely in favour of exclusive price prioritisation. This reflects the desire of the RTB establishment to improve access to premium inventory and obtain first look of all impressions, feeding their insatiable hunger for data.

If header bidding were to be adopted in this manner and scale, publishers would lose control of their data, of how inventory is sold and the ability to make real guarantees. Agencies would have all the data necessary to forecast impression volumes and perform their own segmentation of a publishers audience. Publishers should carefully consider the implications of ceding their remaining control to buyers.

But agencies also need to consider the implications of an entirely price priority controlled future. Although agencies will be able to build their data assets to buy impressions more precisely, agencies will lose important tools used to exert their buying power. Instead, agencies will compete fiercely over tiny pockets of highly valued impressions, but will be unable to provide meaningful guarantees of price or volume to their clients. How will agencies differentiate themselves in such an increasingly commoditised landscape?

Jed Nahum, writes for [AdExchanger](http://adexchanger.com/data-driven-thinking/header-bidding-killed-programmatic-direct/):

> … _RTB has created a war over whether the buy side or the sell side defines what is being sold._ The nutshell of my argument is that RTB has made it possible for buyers to select specific impressions for their buying. This is “decisioning.” In RTB, the buyer does it, and in traditional ad sales the seller does it. I think the real battle is about who gets to decide. Will buyers define the audience they want to reach and bid on it or will sellers define the context they wants to sell and package it up?

It makes lots of sense for increased buyer control over segmentation below guaranteed transactions. Publishers benefit from increased yield, and buyers from an increased pool of impressions. However, reducing all impressions to price priority requires buyers and sellers to surrender control, devalues publisher-agency relationships, and neutralises agency buying power.

Adoption of header bidding is inevitable. What remains to be seen is the speed of adoption and how much control publishers are prepared to give up. I suspect small publishers with limited direct sales have the most to gain from wholesale adoption of header bidding, while larger publishers might see incremental gains below a sub-guaranteed priority implementation, large publishers risk it all by pushing header bidding to higher a priority.
