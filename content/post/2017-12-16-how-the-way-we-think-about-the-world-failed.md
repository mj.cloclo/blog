---

slug: 'how-the-way-we-think-about-the-world-failed'
link: https://eand.co/how-the-way-we-think-about-the-world-failed-5191c8c23d5f
type: post

---


[Umair
Haque](https://eand.co/how-the-way-we-think-about-the-world-failed-5191c8c23d5f) via [The Exponential View](http://mailchi.mp/exponentialview/ev143?e=4e22fd8867)

> In return for a life of insecurity, they have given him more material things. But trading material things for basic rights, dignity, possibility, freedom, justice, truth inevitably results in feelings like anxiety, despair, rage, and shame, which are the discontents of a broken age.

An interesting read, but I doubt many serious intellectuals really think material possessions alone bring happiness. I hope I'm not kidding myself.
