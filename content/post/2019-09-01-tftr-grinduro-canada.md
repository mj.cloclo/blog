---

title: "Tales From The Road: Grinduro Canada"
slug: 'grinduro-canada'
type: post

---

Four [RCCNYC](https://www.rapha.cc/us/en_US/clubhouses/newyork) members, along with members from Washington DC, Boulder and Portland, headed north to Saint-Urbain-de-Charlevoix in search of gravel at the first edition of [Grinduro](http://grinduro.com/canada) in Canada. Part race, part party, Grinduro delivered on all fronts.

![](/uploads/2019/x5n0WFSm5W.jpg)

The 100km race treated us to perfect weather, scenic views, steep gravel climbs and painful miles of sand. All RCCNYC members survived the jumps, tree roots and berms of the timed single track descent on race day (even if not as lucky the day prior when previewing the section). The final major climb – a steep gravel logging road, appearing as if freshly hewn by the bulldozers that littered the roadside – proved challenging for tired legs, but the sight and smell of decks of logs welcomed us to the final summit before the descending to the finish, beer and live music.

The recovery ride was a spirited farewell to the roads of Charlevoix and a welcome opportunity to stretch our legs before driving through the night back to NYC. I'm still not sure if cycling or driving was more exhausting!

