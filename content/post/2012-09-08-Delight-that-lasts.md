---

title: 'Delight that lasts'
people: ['Craig Mod', 'Frank Chimero']
slug: delight-that-lasts
type: post

---

> Don't misconstrue delight and novelty

Consider the moment you first turned a page in Apple iBooks and the novelty of the curling page. If you are anything like me, less than 20 pages later you'd just like to read the next page.

As Craig Mod explains in his talk [Nourishing Habits for Nourishing Designs](https://vimeo.com/33919422) designing for “long term wow“ is hard and delight should not be confused with novelty. Delight—the long term wow—rewards and nourishes users time after time and helps construct a positive relationship with users.

Novelty, in careful and limited use, provides a wonderful treat for the user. The first time they launch your application or visit your webpage you can engage and excite them with a flourish. However, after only a short time novelty fades to frustration.

Designing an interface that delights is hard. It likely won't be noticed the first time the application is used. If well executed, however, the user is rewarded as they return more frequently to the application. The application's predictability and simplicity make it an experience that is good. It delights.

Frank Chimero's [Design Nobility Pyramid](https://vimeo.com/17084347) suggests the highest calling is to first delight users, followed by informing, and lastly persuading.

Superficially this runs counter to most commercial aims. The priority would likely be to first and foremost persuade customers to participate in a revenue generating activity.

However, this assumes that all visitors are equal—new and returning—and that users are inflexible and/or have few alternative service providers. Now more than ever, we should know this to be false where new technology companies gain prominence in timespans measured in weeks and fade to obscurity in days. In light of this developing a relationship, delighting and informing those who use that which we design and create must be our greatest of goals. How else do we create something lasting? How else do we create something of real value?

Attempting to design a product of lasting value and enjoyment is something I wrestle with daily.  Both the technical challenge of designing features that delight users, but more frequently the challenge of scheduling and the cost of development. It is easy to discuss the merit of design nobility and delight in isolation from the cold reality of product management. Far harder is balancing a delightful and feature-rich delivery schedule in a complex and revenue hungry technology start up.

I suppose awareness and the best of intentions are the first step to finding a balance between the two.
