---

slug: 'technological-utopianism'
link: https://logicmag.io/03-dont-be-evil/
type: post

---

[Fred Turner](https://logicmag.io/03-dont-be-evil/) on technological utopianism:

> Any utopianism tends to be a totalizing system. _It promises a total solution to problems that are always piecemeal._ So the problem from my perspective isn't the technological part of technological utopianism but the utopianism part. ... Utopianism, as a whole, is not a helpful approach. Optimism is helpful. But optimism can be partial: it allows room for distress and dismay, it allows room for difference. It's not, as they used to say in the 1960s, all one all the time.

Technology will not solve all the worlds problems – not social media, not bitcoin – but it will be a component of thoughtful positive change.
