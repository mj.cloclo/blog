---

slug: 'one-sentence-email-tips'
link: https://joshspector.com/one-sentence-email-tips/
type: post

---

[40 One-Sentence Email Tips](https://joshspector.com/one-sentence-email-tips/):

> 5. You don’t need to sign your name at the end of your email — the recipient knows who it’s from.

> 13. You don’t always have to reply.

> 23. An email isn’t a letter.

> 40. Unsubscribe.

Solid list. Email is a great medium when used thoughtfully.
