---

title: "GitLab: What we're doing to fix Gitaly NFS performance regressions"
slug: 'gitlab-fixing-nfs-performance'
link: https://about.gitlab.com/2019/07/08/git-performance-on-nfs/
type: post

---

A deep-dive into how _GitLab reduced median latency by 80%_ for `FindCommit` and `TreeEntry` RPCs by sharing a single Git `cat-file` process per Rails session.
