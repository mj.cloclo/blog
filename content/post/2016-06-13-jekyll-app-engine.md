---

title: 'Improving performance of Jekyll with Cloudflare and Google App Engine'
slug: jekyll-app-engine
type: post

---

The thoughts and links I share here are brief. Similarly, I like to keep page load time brief. No one likes to wait for pages to load.

In an experiment to learn more about HTTP/2 Server Push and Google App Engine I moved my blog generated using Jekyll from Github Pages to Google App Engine with Cloudflare in front. A feature of Github Pages is that it 'just works' at the cost of customisation. Google App Engine is a cloud computing platform for web applications and offers complete customisation at the cost of greater complexity, and [Cloudflare supports HTTP/2 Server Push via the `Link` header](https://www.cloudflare.com/http2/server-push/) which is easily specified for static assets served by Google App Engine.

If you're using Chrome 53 or later, the resource inspector shows resources which have been pushed.

![Inspector: blog.jramsay.com.au](http://l.jwr.vc/15B3z+)

While setting up Google App Engine I encountered some challenges using regex url handlers and preferred generating the url handlers as part of the Jekyll build step. After all, Jekyll knows the URL of every page it generates. To do this I wrote a simple Jekyll plugin: [`jekyll-app-engine`](https://github.com/jamesramsay/jekyll-app-engine).

Interestingly while writing the plugin, I discovered Ruby's YAML implementation automatically uses anchors and aliases when writing objects to a YAML string. I quickly discovered this was problematic for my purposes because support for anchors and aliases is [not implemented](https://cloud.google.com/appengine/docs/python/refdocs/modules/google/appengine/api/yaml_builder) in the Google App Engine SDK. This quickly turned my simple adventures in Ruby into a more complex exploration of both the SDK and Ruby's YAML library Psych. In the end I settled for sub-classing Psych's YAMLTree and setting the alias lookup object to a new empty object. If any Rubyists have ideas for a better solution, please let me know.
