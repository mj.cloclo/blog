---

title: 'Header Bidding for consumers'
slug: header-bidding-for-consumers
type: post

---

A few days ago I summarised my thoughts on header bidding. In my brevity, I neglected to reflect on its consumer impact. Alas, this neglect reflects that of the commentary on header bidding, and arguably the systemic neglect of consumers by the advertising industry in general.

Header bidding has two obvious costs to consumers:

- further loss of privacy and anonymity, and
- further reduction of page performance.

Performance problems will surely be solved in time, by relocating complex client-side header bidding scripts to the server-side, but the loss of privacy and anonymity seems only likely to increase.

Considering the hypothetical mass adoption of header bidding and the eradication of the ad server prioritisation, every single impression will be broadcast to the RTB exchanges. Each impression is shown at auction laden with cookies and likely enriched with publisher first-party data. This allows buyers to make educated decisions by only purchasing impressions that have a suitable likelihood of being relevant to the consumer.

One of the advantages to consumers of guaranteed buying, is that a publisher only reveals the customer information to the advertiser once they’ve paid. This makes it harder for any one agency or buyer to build a complete profile of a given consumer. In contrast, RTB is an auction which means sellers will provide information in the hopes of attracting higher bids. Observers may quietly take notes and build a profile, without even having to place a bid.

For my own sake, I hope that header bidding is not adopted quickly or broadly, so that I can continue to enjoy what little privacy I have left. Long term, however, regulation is likely required to control the way in which advertisers and agencies may profile and store consumer data. One such regulation might be to [prohibit companies storing behavioral data longer than 90 days](http://idlewords.com/talks/what_happens_next_will_amaze_you.htm).
