---

title: 'The Truth Delusion of Richard Dawkins'
people: ['Richard Dawkins']
slug: truth-delusion
type: post

---

Melanie Phillips on the intellectual sloppiness of the arch-apostle of reason, Richard Dawkins:

> The way he chose to defend himself, through insults and sneers which tried to cover his tracks as he attempted to retreat from what he had said, furthermore merely emphasised his notable reluctance to address the many arguments of substance against his pseudo-scientific attack on religion which were made by John Lennox on the grounds of scientific reason and accuracy – arguments which Dawkins most tellingly chose to ignore altogether. Instead, he went for what he thought were the soft targets – a credulous Irish Christian and a ‘dreadful woman’ journalist – and substituted smears and jeers for proper debate.

It is worth reading the article in whole, which unfortunately can't be accessed directly at [The Spectator](http://spectator.co.uk) anymore. [[HTML](https://web.archive.org/web/20120121071957/http://www.spectator.co.uk/melaniephillips/3571996/the-truth-delusion-of-richard-dawkins.thtml)] [[PDF](http://l.jwr.vc/9GxA)]

It is good and important to passionately discuss the merits of religion and its compatibility with science and reason, but to sink so low as to lean on gratuitous insult is neither productive nor healthy.
