---

slug: fresh-flowers-in
date: 2020-03-29T07:56:20.923219542Z
photos:
  - /uploads/2020/EJJj5L9Zg.jpg
categories:
  - Photos

---

Fresh flowers in preparation for the week ahead.
