---

title: 'Spotify engineering culture'
link: 'https://labs.spotify.com/2014/03/27/spotify-engineering-culture-part-1/'
slug: spotify-engineering-culture
type: post

---

Interesting that Adslot’s engineering team has independently arrived at a [very similar process and culture to Spotify](https://labs.spotify.com/2014/03/27/spotify-engineering-culture-part-1/). Albeit we’re a much smaller team but many of the ideas Henrik mentions in the video are similar to those we’ve considered in our own context. Maintaining loosely coupled, tightly aligned teams will no doubt become more challenging as team size increases.
